Q: Someone once thought that naming core tools after supermodels was a good idea. Give two names of such commands that were originally part of the Debian Archive Kit (dak), that are still actively used today.
A: Britney, (R)Madison

Q: Cite 5 different valid values for a package's urgency field. Are all of them different?
A: low, medium, high, emergency, critical

Q: What are most Debian project machines named after?
A: composers

Q: What does the B in projectb stand for?
A: Betty
 
Q: Name three locations where Debian machines are hosted.
A: come on you know that

Q: What happened to Debian 1.0?
A: It never happened: Due to an incident involving a CD vendor who made an
unofficial and broken release labeled 1.0, an official 1.0 release was never
made.

Q: Name the first Debian release.
A: Buzz

Q: what are Debian releases named after? Why?
A: Toy story characters. Bruce Perens (DPL, OSI founder) was working for Pixar at the time of Toy Story

Q: Swirl on chin. Does it ring a bell?
A: Buzz!

Q: One Debian release was frozen for more than a year. Which one?
A: Woody

Q: Order correctly hamm, bo, potato, slink
A: bo, hamm, slink, potato

Q: Order correctly lenny, woody, etch, sarge
A: woody, sarge, etch, lenny

Q: what was the first release with an "and-a-half" release?
A: etch

Q: name the kernel version for sarge, etch, lenny, squeeze, wheezy. bonus for etch-n-half!
A: sarge: 2.4.27 + 2.6.8; etch: 2.6.18; lenny: 2.6.26; squeeze: 2.6.32; wheezy: 3.2; etch-n-half: 2.6.24

Q: What was Dunc Tank about? Who was the DPL at the time? Who were the release managers during Dunc Tank?
A: Anthony Towns / Andreas Barth, Steve Langasek

Q: Which one was the Dunc Tank release?
A: etch

Q: Where were the first two DebConf held?
A: Bordeaux, France

Q: Describe the Debian restricted use logo.
A: that's the one with the bottle

Q: What does piuparts stand for?
A: Package Installation, UPgrade, And Removal Testing Suite

Q: What is the codename for experimental?
A: rc-buggy

Q: Which DebConfs were held in a Nordic country?
A: DebConf3 (Oslo, Norway) and DebConf5 (Helsinki, Finland)

Q: What is the official card game at DebConf?
A: (Standard Five Cards DebConf) Mao

Q: When was the Debian Maintainers status created?
A: 2007 (https://www.debian.org/vote/2007/vote_003)

Q: Which Debconf was the first (and so far, only) one hold in winter?
A: debconf8, Argentina, because of southern hemisphere
